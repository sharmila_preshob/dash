//Revenue Chart
new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
    datasets: [{
        data: [30,55,47,80,63,54,70],
        borderColor: "#fb970b",
        fill: false
      },
      {
        data: [25,50,40,70,58,49,65],
        borderColor: "#feeacd",
        fill: false
      }
    ]
  },
  options: {
    legend: {
      display: false
    },
  scales: {
   yAxes: [{
       ticks: {
           min: 20,
           max: 90,
           callback: function(value) {
               return value + "K"
           }
       }
   }]
}
}
});

//Revenue per week Chart
var dataPack1 = [10, 23, 14, 24, 30, 34, 44, 48];
var dataPack2 = [13, 21, 45, 27, 35, 32, 41, 48];
var dataPack3 = [11, 22, 18, 42, 32, 39, 42, 48];
var dataPack4 = [10, 34, 22, 42, 32, 16, 42, 48];
var days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

// Chart.defaults.global.elements.rectangle.backgroundColor = '#FF0000';

var week_ctx_1 = document.getElementById('week-chart-1');
var week_ctx_2 = document.getElementById('week-chart-2');
var week_ctx_3 = document.getElementById('week-chart-3');
var week_ctx_4 = document.getElementById('week-chart-4');

var week_chart_1 = new Chart(week_ctx_1, {
  type: 'line',
  data: {
    labels: days,
    datasets: [{
      data: dataPack1,
      borderColor: "#fb970b",
      fill: false
    }]
  },
  options: {
    legend: {
      display: false
    },
  scales: {
   yAxes: [{
       ticks: {
           min: 10,
           max: 50,
           callback: function(value) {
               return value + "K"
           }
       }
   }]
}
}
});



var week_chart_2 = new Chart(week_ctx_2, {
  type: 'line',
  data: {
    labels: days,
    datasets: [{
      data: dataPack2,
      borderColor: "#fb970b",
      fill: false
    }]
  },
  options: {
    legend: {
      display: false
    },
  scales: {
   yAxes: [{
       ticks: {
           min: 10,
           max: 50,
           callback: function(value) {
               return value + "K"
           }
       }
   }]
}
}
});


var week_chart_3 = new Chart(week_ctx_3, {
  type: 'line',
  data: {
    labels: days,
    datasets: [{
      data: dataPack3,
      borderColor: "#fb970b",
      fill: false
    }]
  },
  options: {
    legend: {
      display: false
    },
  scales: {
   yAxes: [{
       ticks: {
           min: 10,
           max: 50,
           callback: function(value) {
               return value + "K"
           }
       }
   }]
}
}
});

var week_chart_4 = new Chart(week_ctx_4, {
  type: 'line',
  data: {
    labels: days,
    datasets: [{
      data: dataPack4,
      borderColor: "#fb970b",
      fill: false
    }]
  },
  options: {
    legend: {
      display: false
    },
  scales: {
   yAxes: [{
       ticks: {
           min: 10,
           max: 50,
           callback: function(value) {
               return value + "K"
           }
       }
   }]
}
}
});
